import time
import pandas as pd
import numpy as np

CITY_DATA = { 'chicago': 'chicago.csv',
              'new york city': 'new_york_city.csv',
              'washington': 'washington.csv' }

def get_filters():
    print('Hello! Let\'s explore some US bikeshare data!')
    city = input('Enter your city name you need: ').lower()

    while city not in CITY_DATA.keys():
        city = input('Invalid input please Enter your city name you need: ').lower()
    month_option = ['All', 'January', 'February', 'March','April', 'May', 'June']
    month = input("Enter the month you need or 'all' for all months: ").title()

    while month not in month_option:
        month = input("Invalid input please Enter the month you need or 'all' for all months: ").title()
    day_option = ['All', 'Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
    day = input("Enter the day you need or 'all' for all days: ").title()

    while day not in day_option:
        day = input("Invalid input please Enter the day you need or 'all' for all days: ").title()
    print('-'*40)

    return city, month, day
    
def load_data(city, month, day):
    df = pd.read_csv(CITY_DATA[city])
    df['Start Time'] = pd.to_datetime(df['Start Time'])
    df['month'] = df['Start Time'].dt.month_name()
    df['day'] = df['Start Time'].dt.day_name()

    if month != 'All' and day != 'All':
        df = df[df['month'] == month]
        df = df[df['day'] == day ]
    elif month != 'All':
        df = df[df['month'] == month]     
    elif day != 'All':
        df = df[df['day'] == day ]

    return df

def time_stats(df):
    """Displays statistics on the most frequent times of travel."""
    print('\nCalculating The Most Frequent Times of Travel...\n')
    start_time = time.time()
    df['Start Time'] = pd.to_datetime(df['Start Time'])

    # TO DO: display the most common month
    df['month'] = df['Start Time'].dt.month_name()
    common_month = df['month'].mode()[0]

    # TO DO: display the most common day of week
    df['day'] = df['Start Time'].dt.day_name()
    common_day = df['day'].mode()[0]

    # TO DO: display the most common start hour
    df['hour'] = df['Start Time'].dt.hour
    common_hour = df['hour'].mode()[0]
    print('the most common month is {} , the most common day of week is {} and the most common start hour is {}'.format(common_month,common_day,common_hour))
    print("\nThis took %s seconds." % (time.time() - start_time))
    print('-'*40)


def station_stats(df):
    """Displays statistics on the most popular stations and trip."""
    print('\nCalculating The Most Popular Stations and Trip...\n')
    start_time = time.time()

    # TO DO: display most commonly used start station
    common_start_station = df['Start Station'].mode()[0]
    print('the common start station is {}'.format(common_start_station))

    # TO DO: display most commonly used end station
    common_end_station = df['End Station'].mode()[0]
    print('the common end station is {}'.format(common_end_station))

    # TO DO: display most frequent combination of start station and end station trip
    df['trip'] = df['Start Station'] + ' to ' + df['End Station']
    most_frequent_trip = df['trip'].mode()[0]
    print('the most frequent trip is {}'.format(most_frequent_trip))
    print("\nThis took %s seconds." % (time.time() - start_time))
    print('-'*40)


def trip_duration_stats(df):
    """Displays statistics on the total and average trip duration."""
    print('\nCalculating Trip Duration...\n')
    start_time = time.time()

    # TO DO: display total travel time
    total_trip_duration = df['Trip Duration'].sum()

    # TO DO: display mean travel time
    average_trip_duration = df['Trip Duration'].mean()
    print('total trip duration is {} and average trip duration is {}'.format(total_trip_duration,average_trip_duration))
    print("\nThis took %s seconds." % (time.time() - start_time))
    print('-'*40)


def user_stats(df):
    """Displays statistics on bikeshare users."""
    print('\nCalculating User Stats...\n')
    start_time = time.time()
    customer_count = 0
    subscriber_count = 0

    # TO DO: Display counts of user types
    for i in df['User Type']:
        if i == 'Subscriber':
            subscriber_count += 1
        elif i == 'Customer':
            customer_count += 1
    print('subscriber count is {} and customer count is {}'.format(subscriber_count , customer_count)) 

    # TO DO: Display counts of gender
    try:
        male_count = 0
        female_count = 0
        for i in df['Gender']:
            if i == 'Male':
                male_count += 1
            elif i == 'Female':
                female_count += 1
        print('male count is {} and female count is {}'.format(male_count , female_count))
    except:
        print('Sorry this city hasn\'t gender data')    

    # TO DO: Display earliest, most recent, and most common year of birth
    try:
        early_bd = int(df['Birth Year'].min())
        recent_bd = int(df['Birth Year'].max())
        common_year = int(df['Birth Year'].mode()[0])
        print('earliest year of birth is {} , most recent year of birth is {} and common year of birth is {}'.format(early_bd,recent_bd,common_year))
    except:
        print('Sorry this city hasn\'t birth data')
    print("\nThis took %s seconds." % (time.time() - start_time))
    print('-'*40)

def display_raw_data(df):
    view_data = input('Do you want to see the first 5 rows of data? please write yes / no ').lower()
    start_loc = 0
    if view_data == 'yes':
        while view_data == 'yes':
            print(df.iloc[start_loc:start_loc+5])
            start_loc += 5
            view_data = input('Do you want to see the next 5 rows of data? please write yes / no ').lower()

def main():
    while True:
        city, month, day = get_filters()
        df = load_data(city, month, day)

        time_stats(df)
        station_stats(df)
        trip_duration_stats(df)
        user_stats(df)
        display_raw_data(df)
        restart = input('\nWould you like to restart? Enter yes or no.\n')
        if restart.lower() != 'yes':
            break


if __name__ == "__main__":
	main()